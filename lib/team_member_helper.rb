# frozen_string_literal: true

module TeamMemberHelper
  def has_specialty?(username, specialty)
    team_member_data = WwwGitLabCom.team_from_www[username]

    return false if team_member_data.nil?

    specialties = team_member_data.fetch('specialty', [])

    Array(specialties).include?(specialty)
  end
end
