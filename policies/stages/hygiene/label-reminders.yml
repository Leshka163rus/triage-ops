.regression_missing_labels:
  - &regression_label_missing
    name: Label missed ~regression for ~regression:x.y
    conditions:
      state: opened
      forbidden_labels:
        - regression
      labels:
        - regression:{14..17}.{0..15}
    actions:
      labels:
        - regression
        - type::bug

  - &bug_label_missing
    name: Label missed ~bug for ~regression
    conditions:
      state: opened
      forbidden_labels:
        - type::bug
      labels:
        - regression
    actions:
      labels:
        - type::bug

.broken_master_missing_labels:
  - &priority_severity_labels_missing_for_broken_master
    name: Severity/priority labels missing for ~master:broken
    conditions:
      state: opened
      date:
        attribute: created_at
        condition: newer_than
        interval_type: months
        interval: 1
      labels:
        - master:broken
      forbidden_labels:
        - severity::1
        - priority::1
    actions:
      labels:
        - severity::1
        - priority::1

resource_rules:
  issues:
    rules:
      - *regression_label_missing
      - *bug_label_missing
      - *priority_severity_labels_missing_for_broken_master

      - name: Remind team members to set relevant labels on their Issues
        conditions:
          author_member:
            source: group
            condition: member_of
            # gitlab-org
            source_id: 9970
          labels:
            - none
          state: opened
        actions:
          labels:
            - auto updated
          comment: |
            {{author}}, please [add labels to your issue](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#labels). Labels help us find and categorize issues.

            Thanks for your help! :heart:

            ---

            ([improve this comment?](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/stages/hygiene/label-reminders.yml))

      - name: Label ~regression:x.y for ~regression where x.y is current version
        conditions:
          state: opened
          ruby: |
            resource[:labels].grep(/^regression:/).empty?
          labels:
            - regression
        actions:
          labels:
            - type::bug
          comment: |
            This issue is labeled ~regression, but doesn't specify which milestone introduced it. We assume it was introduced in the current version (#{instance_version.version}) and have labeled it ~"regression:#{instance_version.version_short}":

            1. If this version number is wrong, please correct it.
            1. Keep the ~regression label. It helps us search for regressions across all versions.

            /label ~"regression:#{instance_version.version_short}"

      - name: Categorize Geo issues
        conditions:
          state: opened
          labels:
            - Geo
          forbidden_labels:
            - Geo Verification/Accuracy
            - Geo Replication/Sync
            - Geo Performance
            - Geo Administration
            - Geo DR
            - UX
            - backstage
            - maintenance::refactor
            - auto updated
            - triage report
        limits:
          most_recent: 20
        actions:
          labels:
            - auto updated
          comment: |
            Hi {{author}},

            This issue is labelled ~Geo and we would like some more information about it.

            Please add any of these labels to help categorize this issue:
            - ~"Geo Verification/Accuracy"
            - ~"Geo Replication/Sync"
            - ~"Geo Performance"
            - ~"Geo Administration"
            - ~"Geo DR"
            - ~"UX"
            - ~"backstage"
            - ~"maintenance::refactor"

  merge_requests:
    rules:
      - *regression_label_missing
      - *bug_label_missing
      - *priority_severity_labels_missing_for_broken_master

      - name: Remind team members to set relevant labels on their MRs
        conditions:
          author_member:
            source: group
            condition: member_of
            # gitlab-org
            source_id: 9970
          labels:
            - none
          state: opened
        actions:
          labels:
            - auto updated
          comment: |
            Hi {{author}},

            Please [add labels to your merge request](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#labels) if you can. Labels help us triage community merge requests.

            Thanks for your help! :heart:

            ---

            ([Improve this comment](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/stages/hygiene/label-reminders.yml)?)
