# frozen_string_literal: true

require_relative '../appsec_processor'

module Triage
  class AppSecApprovalLabelAdded < AppSecProcessor
    react_to 'merge_request.update', 'merge_request.open'

    def applicable?
      super && appsec_approval_label_added?
    end

    def process
      if approved_by_appsec?
        discussion_id = add_to_appsec_thread(message)

        resolve_discussion(discussion_id)
      else
        # TODO: Dis-approve. Not done by appsec!
        # https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/963
      end
    end

    private

    def appsec_approval_label_added?
      event.added_label_names.include?(APPSEC_APPROVAL_LABEL)
    end

    def message
      @message ||= unique_comment.wrap(<<~MARKDOWN.strip)
        :white_check_mark: AppSec added ~"#{APPSEC_APPROVAL_LABEL}" for
        #{event.last_commit_sha} by `@#{event.event_actor_username}`
      MARKDOWN
    end
  end
end
