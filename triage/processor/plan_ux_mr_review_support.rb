# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/processor'
require_relative '../triage/unique_comment'

module Triage
  # Temporary support for Alexis https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/879
  class PlanUxMrReviewSupport < Processor
    UX_LABEL = 'UX'
    PLAN_LABEL = 'devops::plan'
    REVIEWERS = %w[@uhlexsis @andyvolpe @beckalippert @mfangman].freeze

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      event.from_gitlab_org? &&
        event.resource_open? &&
        !event.wip? &&
        ux_label_added? &&
        plan_label_added? &&
        unique_comment.no_previous_comment?
    end

    def process
      post_review_command
    end

    private

    def ux_label_added?
      event.label_names.include?(UX_LABEL)
    end

    def plan_label_added?
      event.label_names.include?(PLAN_LABEL)
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def post_review_command
      add_comment(message.strip)
    end

    def message
      comment = <<~MESSAGE
        Thanks for helping us improve the UX of GitLab! We have automatically assigned a UX reviewer for this merge request.
        /assign_reviewer #{REVIEWERS.sample}
      MESSAGE

      unique_comment.wrap(comment)
    end
  end
end
